<?php

use Crim\Authorization\app\Http\Controllers\ClientsController;
use \Illuminate\Support\Facades\Redirect;

Route::group(['prefix' => '/auth', 'middleware' => ['web']], function() {
    Route::get('/', function() {
        return Redirect::to('auth/login');
    });

    Route::group(['prefix' => '/login'], function() {
        /*TODO: REFACTOR LONG PATH FOR CONTROLLERS*/
        Route::get('/', 'Crim\Authorization\app\Http\Controllers\AuthLoginController@userLoginForm');
        Route::post('/', 'Crim\Authorization\app\Http\Controllers\AuthLoginController@userLogin');
    });

    Route::group(['prefix' => '/register'], function() {
        /*TODO: REFACTOR LONG PATH FOR CONTROLLERS*/
        Route::get('/', 'Crim\Authorization\app\Http\Controllers\AuthRegisterController@userRegisterForm');
        Route::post('/', 'Crim\Authorization\app\Http\Controllers\AuthRegisterController@userRegister');
    });

});
