<?php


namespace Crim\Authorization\app\Http\Controllers;


use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class AuthRegisterController extends RegisterController
{
    public function __construct()
    {
        /*TODO: Put secuity*/
//        $this->middleware('Role:admin', ['only' => ['login']]);
    }

    public function userRegisterForm()
    {
        return view('crim::authorization/register');
    }

    public function userRegister(Request $request)
    {
        $this->register($request);

        return Redirect::to($this->redirectTo);
    }

}
