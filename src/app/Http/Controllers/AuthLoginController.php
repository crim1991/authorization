<?php


namespace Crim\Authorization\app\Http\Controllers;


use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class AuthLoginController extends LoginController
{
    public function __construct()
    {
        /*TODO: Put secuity*/
//        $this->middleware('Role:admin', ['only' => ['login']]);
    }

    public function userLoginForm()
    {
        return view('crim::authorization/login');
    }

    public function userLogin(Request $request)
    {
        $this->login($request);
        return Redirect::to($this->redirectTo);
    }
}
