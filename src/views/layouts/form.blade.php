<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Log In | UBold - Responsive Admin Dashboard Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('vendor/authorization/images/favicon.ico') }}">
    <!-- App css -->
    <link href="{{ asset('vendor/authorization/css/bootstrap-creative.min.css') }}" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
    <link href="{{ asset('vendor/authorization/css/app-creative.min.css') }}" rel="stylesheet" type="text/css" id="app-default-stylesheet" />
    <link href="{{ asset('vendor/authorization/css/bootstrap-creative-dark.min.css') }}" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" />
    <link href="{{ asset('vendor/authorization/css/app-creative-dark.min.css') }}" rel="stylesheet" type="text/css" id="app-dark-stylesheet" />
    <!-- icons -->
    <link href="{{ asset('vendor/authorization/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
</head>

<body class="loading authentication-bg authentication-bg-pattern">
    @yield('content')
<footer class="footer footer-alt text-white-50">
    2015 - <script>document.write(new Date().getFullYear())</script> &copy; UBold theme by <a href="auth-login.html" class="text-white-50">Coderthemes</a>
</footer>

<!-- Vendor js -->
<script src="{{ asset('vendor/authorization/js/vendor.min.js') }}"></script>

<!-- App js -->
<script src="{{ asset('vendor/authorization/js/app.min.js') }}"></script>

</body>
</html>
